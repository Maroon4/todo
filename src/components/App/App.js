import React, {Component} from 'react';
import ReactDom from 'react-dom'
import MyHeader from '../MyHeader'
import MySearch from '../MySearch'
import MyTasksList from '../MyTasksList'
import ItemStatusFilter from '../ItemStatusFilter'
import AddNewList from '../AddNewList'
import './App.css'



export default class App extends Component{

    maxId = 100;

    state = {
        todoData: [
            this.createNewItem('Drink Coffe'),
            this.createNewItem("Make My App"),
            this.createNewItem("Have a lunch"),
        ],

        mySearch: '',

        filter: ''
    };


    deletedItem = (id) => {
        this.setState(({ todoData }) => {
            const idx = todoData.findIndex((el) => el.id === id);

            const newArray = [
                ...todoData.slice(0, idx),
                ...todoData.slice(idx + 1)
            ];

            return {
                todoData: newArray
            };
        });
    };

    createNewItem (label) {
        return {
            label,
            done: false,
            important: false,
            id: this.maxId++
        }

    };

    addItem = (text) => {
     const newItem = this.createNewItem(text);

     this.setState(({todoData}) => {

         const newArr = [
             ...todoData,
             newItem
         ];
         return {
            todoData: newArr
         }
     })
    };

    onTuggle (arr, id, propName) {
        const idx = arr.findIndex((el) => el.id === id);
        const oldItem = arr[idx];

        const newItem = {...oldItem, [propName]: !oldItem[propName]};

        return [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ];
    }

    onTuggleDone = (id) => {
       this.setState(({todoData}) => {
           return {
               todoData: this.onTuggle(todoData, id, 'done')
           }
       })
    };

    onTuggleImportant = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.onTuggle(todoData, id, "important")
            }
        });
    };

    onLabelSearch(items, mySearch) {
        if (mySearch.length === 0) {
            return items;
        }
        return  items.filter((item) => {
            return item.label.toLowerCase()
                .indexOf(mySearch.toLowerCase()) > -1;
        });

    };

    onSearchChange = (mySearch) => {
        this.setState({mySearch});
    };

    onFilterChange = (filter) => {
        this.setState({filter});
    };

    filter = (items, filter) => {

        switch (filter) {
            case 'all':
                return items;
            case 'active':
                return items.filter((item) => !item.done);
            case 'done':
                return items.filter((item) => item.done);
            default:
                return items;

        }

    };



    render() {

        const {todoData, mySearch, filter} = this.state;

        const visibleItems = this.filter(
            this.onLabelSearch(todoData, mySearch), filter);

        const doneCount = todoData
            .filter((el) => el.done).length;
        const todoCount = todoData.length - doneCount;

        return (
            <div className= "todo-app">
                <MyHeader toDo={todoCount} done={doneCount}/>
                <div className="top-panel d-flex">
                    <MySearch
                        onSearchChange = {this.onSearchChange}
                    />
                    <ItemStatusFilter
                        filters = {this.state.filter}
                        onFilterChange = {this.onFilterChange}/>
                </div>
                <MyTasksList
                    todos = {visibleItems}
                    onDeleted = { this.deletedItem }
                    onTuggleDone = {this.onTuggleDone}
                    onTuggleImportant = {this.onTuggleImportant}/>
                <AddNewList onItemAdd={this.addItem}/>

            </div>
        );
    }



};

