
import React, {Component} from 'react'
import './AddNewList.css'

export default class AddNewList extends Component{

    state = {
        label: ''
    };

    onChange = (e) => {
        this.setState( {
            label: e.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        const {onItemAdd} = this.props;
        onItemAdd(this.state.label);
        this.setState({
            label: ''
        });
    };


    render() {
        return (
            <form className="bottom-panel d-flex"
                  onSubmit={this.onSubmit}>
                <input
                    type="text"
                    className="form-control AddNewList"
                    placeholder="add new list"
                    onChange={this.onChange}
                    value={this.state.label}/>
                <button type="button"
                        className="btn btn-outline-secondary"
                        onClick={this.onSubmit}>Add</button>
            </form>
        )
    }


}

