import React, {Component} from 'react'

import "./my-task.css"

export default class MyTask1 extends Component {


    render() {
        const {label, onDeleted,
               onTuggleDone,
               onTuggleImportant,
               important, done} = this.props;

        let classNames = 'my-task';
        if (done) {
            classNames += ' done';
        }
        if (important) {
            classNames += ' important';
        }

            return (
                   <span className={classNames}>
                      <span
                          className="my-task-label"
                          onClick={ onTuggleDone} >
                       {label}
                       </span>

                          <button type="button"
                                  className="btn btn-outline-success btn-sm float-right"
                          onClick={ onTuggleImportant }>
                       <i className="fa fa-exclamation"/>
                          </button>

                       <button type="button"
                               className="btn btn-outline-danger btn-sm float-right"
                               onClick={onDeleted}>
                       <i className="fa fa-trash-o"/>
                      </button>
                    </span>
                    );

                };
              }

