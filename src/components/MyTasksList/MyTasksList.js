import React from 'react'
import MyTask1 from '../MyTasks1'
import "./MyTasksList.css"

const MyTasksList = ( {todos, onDeleted, onTuggleDone, onTuggleImportant, filter} ) => {

const elements = todos.map( (items) => {
     const {id , ...itemsProps} = items;
    return (
            <li key={id} className = "mytaskslist">
                <MyTask1 { ...itemsProps }
                         onDeleted = {() => onDeleted(id)}
                         onTuggleDone={() => onTuggleDone(id)}
                         onTuggleImportant={() => onTuggleImportant(id)}/>
            </li>

        );

}

)

       return (
           <ul className="list-group mytaskslist">
               {elements}
           </ul>
       );

};

export default MyTasksList;